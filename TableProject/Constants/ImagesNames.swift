import Foundation

struct ImagesNames {
    static let psychology = "psychology"
    static let flutter = "flutter"
    static let home = "home"
    static let lightbulb = "lightbulb"
    static let lock = "lock"
    static let notifications = "notifications"
    static let pets = "pets"
    static let rocket = "rocket"
    static let star = "star"
    static let touch = "touch"
}
