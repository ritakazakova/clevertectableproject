import Foundation

struct TextConstants {
    static let textTitle = "Title"
    static let textDescription = "Description"
}
