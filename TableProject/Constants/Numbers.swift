import Foundation

struct NumbersConstants {
    static let sizeViewUnderCellsImage = 50
    static let spaceImageToView = 5
    static let spaceImageViewToView = 10
}
