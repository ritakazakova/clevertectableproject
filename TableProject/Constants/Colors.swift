import UIKit

struct ColorConstants {
    static let darkGreyColor: UIColor = UIColor(red: 0.35, green: 0.4, blue: 0.42, alpha: 1)
    static let lightGreyColor: UIColor = UIColor(red: 0.75, green: 0.77, blue: 0.79, alpha: 1)
}
