import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let identifier = "cell"
    private var cellsElements = [ItemCellModel]()
    private lazy var iconImageView: UIView = {
        let imageView = UIView()
        imageView.layer.cornerRadius = CGFloat(NumbersConstants.sizeViewUnderCellsImage / 2)
        imageView.layer.backgroundColor = ColorConstants.lightGreyColor.cgColor
        return imageView
    }()
    private(set) lazy var iconImage: UIImageView = {
        let iconImage = UIImageView()
        iconImage.tintColor = ColorConstants.darkGreyColor
        return iconImage
    }()
    private(set) lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textColor = ColorConstants.darkGreyColor
        return titleLabel
    }()
    private(set) lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textColor = ColorConstants.lightGreyColor
        return descriptionLabel
    }()
    private(set) lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 3
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(iconImageView)
        iconImageView.addSubview(iconImage)
        contentView.addSubview(labelsStackView)
        
        constraintsIconImageSetup()
        constraintsIconImageViewSetup()
        constraintsLabelsSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomTableViewCell {
    private func constraintsIconImageSetup() {
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        
        iconImage.leadingAnchor.constraint(equalTo: iconImageView.leadingAnchor, constant: CGFloat(NumbersConstants.spaceImageToView)).isActive = true
        iconImage.trailingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: CGFloat(-NumbersConstants.spaceImageToView)).isActive = true
        iconImage.topAnchor.constraint(equalTo: iconImageView.topAnchor, constant: CGFloat(NumbersConstants.spaceImageToView)).isActive = true
        iconImage.bottomAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: CGFloat(-NumbersConstants.spaceImageToView)).isActive = true
    }
    
    private func constraintsIconImageViewSetup() {
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        
        iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: CGFloat(NumbersConstants.spaceImageViewToView)).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        iconImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: CGFloat(NumbersConstants.spaceImageViewToView)).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: CGFloat(NumbersConstants.sizeViewUnderCellsImage)).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: CGFloat(NumbersConstants.sizeViewUnderCellsImage)).isActive = true
    }
    
    private func constraintsLabelsSetup() {
        labelsStackView.translatesAutoresizingMaskIntoConstraints = false

        labelsStackView.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 20).isActive = true
        labelsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15).isActive = true
        labelsStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15).isActive = true
        labelsStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25).isActive = true
    }
}
