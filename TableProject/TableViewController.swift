import UIKit

class TableViewController: UIViewController {
    
    private let images = [ImagesNames.psychology, ImagesNames.flutter, ImagesNames.home, ImagesNames.lightbulb, ImagesNames.lock, ImagesNames.notifications, ImagesNames.pets, ImagesNames.rocket, ImagesNames.star, ImagesNames.touch]
    private var cellsElements = [ItemCellModel]()
    private lazy var mainTableView: UITableView = {
        let table = UITableView()
        table.dataSource = self
        table.delegate = self
        table.isEditing = true
        table.allowsSelectionDuringEditing = true
        return table
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0..<1000 {
            let indexOfImage = i % images.count
            cellsElements.append(ItemCellModel(title: "\(TextConstants.textTitle) \(i+1)",
                                               description: "\(TextConstants.textDescription) \(i+1)",
                                               image: UIImage(named: "\(images[indexOfImage])")))
        }
        
        mainTableView.register(CustomTableViewCell.self, forCellReuseIdentifier: CustomTableViewCell.identifier)
        view.backgroundColor = .white
        view.addSubview(mainTableView)
        
        constraintsTableViewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension TableViewController: UITableViewDelegate, UITableViewDataSource {
    private func constraintsTableViewSetup() {
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        
        mainTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mainTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        mainTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let secondViewController = SecondaryViewController()

        self.navigationController?.pushViewController(secondViewController, animated: true)
        self.navigationController?.navigationBar.tintColor = ColorConstants.darkGreyColor
        
        tableView.deselectRow(at: indexPath, animated: true)
        secondViewController.imageFromCell.image = cellsElements[indexPath.row].image
        secondViewController.titleLabel.text = "\(cellsElements[indexPath.row].title)"
        secondViewController.descriptionLabel.text = "\(cellsElements[indexPath.row].description)"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsElements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.identifier,
                                                 for: indexPath) as! CustomTableViewCell
        cell.titleLabel.text = "\(cellsElements[indexPath.row].title)"
        cell.descriptionLabel.text = "\(cellsElements[indexPath.row].description)"
        cell.iconImage.image = cellsElements[indexPath.row].image

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            cellsElements.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = cellsElements[sourceIndexPath.row]
        cellsElements.remove(at: sourceIndexPath.row)
        cellsElements.insert(movedObject, at: destinationIndexPath.row)
        tableView.reloadData()
    }
}

