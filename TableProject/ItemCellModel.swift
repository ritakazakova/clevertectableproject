import UIKit

struct ItemCellModel {
    let title: String
    let description: String
    let image: UIImage?
}

