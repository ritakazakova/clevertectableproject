import UIKit

class SecondaryViewController: UIViewController {
    private lazy var iconsImageView: UIView = {
        let imageView = UIView()
        imageView.layer.cornerRadius = CGFloat(NumbersConstants.sizeViewUnderCellsImage)
        imageView.layer.backgroundColor = ColorConstants.lightGreyColor.cgColor
        return imageView
    }()
    private(set) lazy var imageFromCell: UIImageView = {
        let image = UIImageView()
        image.tintColor = ColorConstants.darkGreyColor
        return image
    }()
    private(set) lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textColor = ColorConstants.darkGreyColor
        return titleLabel
    }()
    private(set) lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.textColor = ColorConstants.lightGreyColor
        return descriptionLabel
    }()
    private(set) lazy var labelsStackView: UIStackView = {
    let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(iconsImageView)
        iconsImageView.addSubview(imageFromCell)
        view.addSubview(labelsStackView)
        
        constraintsImageSetup()
        constraintsLabelsSetup()
        constraintsIconsImageViewSetup()
    }
}

extension SecondaryViewController {
    private func constraintsImageSetup() {
        imageFromCell.translatesAutoresizingMaskIntoConstraints = false
        
        imageFromCell.trailingAnchor.constraint(equalTo: iconsImageView.trailingAnchor, constant: CGFloat(-NumbersConstants.spaceImageToView * 2)).isActive = true
        imageFromCell.bottomAnchor.constraint(equalTo: iconsImageView.bottomAnchor, constant: CGFloat(-NumbersConstants.spaceImageToView * 2)).isActive = true
        imageFromCell.leadingAnchor.constraint(equalTo: iconsImageView.leadingAnchor, constant: CGFloat(NumbersConstants.spaceImageToView * 2)).isActive = true
        imageFromCell.topAnchor.constraint(equalTo: iconsImageView.topAnchor, constant: CGFloat(NumbersConstants.spaceImageToView * 2)).isActive = true
    }
    
    private func constraintsIconsImageViewSetup() {
        iconsImageView.translatesAutoresizingMaskIntoConstraints = false
        
        iconsImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        iconsImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iconsImageView.heightAnchor.constraint(equalToConstant: CGFloat(NumbersConstants.sizeViewUnderCellsImage * 2)).isActive = true
        iconsImageView.widthAnchor.constraint(equalToConstant: CGFloat(NumbersConstants.sizeViewUnderCellsImage * 2)).isActive = true
    }
    
    private func constraintsLabelsSetup() {
        labelsStackView.translatesAutoresizingMaskIntoConstraints = false

        labelsStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        labelsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
 
